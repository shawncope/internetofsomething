#!/bin/bash

# Set the ROOM_ID & AUTH_TOKEN variables below.
# Further instructions at https://www.hipchat.com/docs/apiv2/auth

# Usage: ./hipchat_success.sh "path/to/QB/configuration" buildnumber
# Hint: Use ${configuration.pathName} and ${build.version} in QuickBuild to populate these values.
# Example: /opt/quickbuild/scripts/hipchat_success.sh "${configuration.pathName}" ${build.version}

BUILD_PATH="${1}"
BUILD_VERSION="${2}"
ROOM_ID=<hipchat room id here>
AUTH_TOKEN=<hipchat auth token here>

curl -H "Content-Type: application/json" \
     -X POST \
     -d "{\"color\": \"red\", \"message_format\": \"text\", \"message\": \"$BUILD_PATH version $BUILD_VERSION has failed. (rageguy)\" }" \
     https://api.hipchat.com/v2/room/$ROOM_ID/notification?auth_token=$AUTH_TOKEN
