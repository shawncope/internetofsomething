// include the neo pixel library
#include <Adafruit_NeoPixel.h>
#include <ArduinoJson.h>

Adafruit_NeoPixel strip = Adafruit_NeoPixel(8, 6);
//const int bufferSize = 6;
const int bufferSize = 1280;
char json[bufferSize];
char colorValues[bufferSize];
int totalLeds = strip.numPixels();

void setPixelColor(Adafruit_NeoPixel &ledstrip, uint16_t n, uint8_t r, uint8_t g, uint8_t b, uint8_t brightness) {
  r = (r * (brightness / 255.0));
  g = (g * (brightness / 255.0));
  b = (b * (brightness / 255.0));
  
  ledstrip.setPixelColor(n, r, b, g);
  strip.show();
}

void setup() {
  Serial.begin(56000);
  while (!Serial) {
    // wait serial port initialization
  }

  strip.begin();
  
  for(int i=0;i<totalLeds;i++) {
    strip.setPixelColor(i, strip.Color(0,0,0));
    strip.show();
  }
}

void loop() {

}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  char buffer[bufferSize];
  static size_t pos;              // position of next write
  
  while (Serial.available() && pos < sizeof buffer - 1) {

    // Read incoming byte.
    char c = Serial.read();
    buffer[pos++] = c;

    // Echo received message.
    if (c == '|') {            // \n means "end of message"
      buffer[pos--] = '\0';     // terminate the buffer
      Serial.println(buffer);   // send echo
      //processSerialData(buffer);
      uint16_t led = (buffer[1] << 8) | buffer[0];        
      uint8_t red = buffer[2]; // 0 - 255, 1 byte
      uint8_t green = buffer[3]; // 0 - 255, 1 byte
      uint8_t blue = buffer[4]; // 0 - 255, 1 byte
      uint8_t brightness = buffer[5]; // 1 byte

      setPixelColor(strip, led, red, blue, green, brightness);
      
      pos = 0;                // reset to start of buffer
    }
  }
}


