var serialport = require("serialport");
    //SerialPort = serialport.SerialPort;
var serial;
var http = require("http");
var url = require('url') ;
var port = null;
var bufferSeparater = new Buffer("|");

var looperInterval, looperTimer;


var server = http.createServer(function(request, response) {
	if (request.url === '/favicon.ico') {
		response.writeHead(200, {'Content-Type': 'image/x-icon'} );
		response.end();
		//console.log('favicon requested');
		return;
	}
	
	var queryObject = url.parse(request.url,true).query;
  	//console.log(queryObject);
	if(queryObject.hasOwnProperty('color') && queryObject.color != '');
		changeLight(getColor(queryObject.color));
	
	response.writeHead(200, {"Content-Type": "text/html"});
	response.write('<!DOCTYPE "html">');
	response.write("<html>");
	response.write("<head>");
	response.write("<title>Hello World Page</title>");
	response.write("</head>");
	response.write("<body>");
	response.write("Hello World!");
	response.write("</body>");
	response.write("</html>");
	response.end();
	//console.log("request", request);
});
server.listen(8080);
console.log("Server is listening");

// first list the serial ports available so we can figure out which is the arduino
serialport.list(function (err, ports) {

    ports.forEach(function(p) {
        // this should work on windows and maybe osx
        if (p.manufacturer.indexOf('Arduino')!==-1) {
            port = p.comName;
        } else {
            // this will work on raspberry pi / linux
            if (p.hasOwnProperty('pnpId')){
                // FTDI captures the duemilanove //
                // Arduino captures the leonardo //
                if (p.pnpId.search('FTDI') != -1 || p.pnpId.search('Arduino') != -1) {
                    port = p.comName;
                }
            }
        }
    });
	
	// port should now contain a string for the com port
	// open the port
	serial = new serialport.SerialPort(port, {
		baudrate: 56000,
		//encoding: "utf8"
		parser: serialport.parsers.readline('|')
	});

	// hook up open event
	serial.on("open", function () {

		setTimeout(function(){
			// port is open
			console.log('port ' + port + ' opened');
			// hook up data listener to echo out data as its received
			serial.on('data', function(data) {
				console.log('data received: ' + data.toString());
			});

			changeLight(lightRed, port);

		},3500);
	});
});

	



process.on('SIGINT', function() {

	changeLight(darkLEDS());
	
	setTimeout(function(){
		serial.close();
		process.exit();
	},750);
});

var darkLEDS = function(numLeds) {
	numLeds = numLeds || 12;
	
	this.data = [];
		
	for(var i=0; i<numLeds; i++){
		this.data.push({
			led: i,
			r: 0,
			g: 0,
			b: 0
		});
	}
	
	return this.data;
};

var changeLight = function(colorArray) {
	//console.log('changeLight', colorArray);
	var buffer = new Buffer('');
	
	for(var i=0;i<colorArray.length;i++) {
		var led = new Uint16Array(1);
		var color = new Uint8Array(4);

		led[0] = colorArray[i].led; // LED ID - 2 bytes
		color[0] = colorArray[i].r; // Red - 1 byte
		color[1] = colorArray[i].g; // Green - 1 byte
		color[2] = colorArray[i].b; // Blue - 1 byte
		color[3] = colorArray[i].i; // Brightness - 1 byte

		var ledbuffer = new Buffer(led.buffer);
		var colorbuffer = new Buffer(color.buffer);
		buffer = Buffer.concat([buffer, ledbuffer, colorbuffer, bufferSeparater])
	}
	
	serial.write(buffer);
}

var setAllTheSameColor = function(color, numLeds) {
	//console.log('setAllTheSameColor', color, numLeds);
	color = color || {r:0, g:0, b:0, i:0};
	numLeds = numLeds || 12;
	var colorArray = [];
	
	for(var i=0; i<numLeds; i++) {
		colorArray.push({
			led: i,
			r: color.r,
			g: color.g,
			b: color.b,
			i: color.i
		});
	}
	
	return colorArray;
}

var getColor = function(color) {
	//console.log('getColor', color);
	if(color === 'green')
		return lightGreen;
	if(color === 'yellow')
		return lightYellow;
	if(color === 'red')
		return lightRed;
	if(color === 'off')
		return off;
}

var lightRed = setAllTheSameColor({r:255, g:0, b:0, i:255});

var lightYellow = setAllTheSameColor({r:255, g:255, b:0, i:255});

var lightGreen = setAllTheSameColor({r:0, g:255, b:0, i:255});

var off = setAllTheSameColor({r:0, g:0, b:0, i:0});

var roygbiv = [
	{led: 0, r: 255, g: 0, b: 0, i: 255},// Red
	{led: 1, r: 255, g:127, b: 0, i: 255},// Orange
	{led: 2, r: 255, g: 255, b: 0, i: 255},// Yellow
	{led: 3, r: 0, g: 255, b: 0, i: 255},// Green
	{led: 4, r: 0, g: 0, b: 255, i: 255},// Blue
	{led: 5, r: 50, g: 0, b: 255, i: 255},// Indigo
	{led: 6, r: 139, g: 0, b: 255, i: 255},// Violet
	{led: 7, r: 255, g: 255, b: 255, i: 255}// White
];